const fs = require('fs');

require('dotenv').config();
function conexionsqlserver() {
    const rest = new (require('rest-mssql-nodejs'))({
        user: process.env.SERVERSQL_USER,
        password: process.env.SERVERSQL_PWD,
        server: process.env.SERVERSQL_IP,
        database : process.env.SERVERSQL_DB,
        port: parseInt(process.env.SERVERSQL_PORT)
    });
    return rest;
}


function padLeft(nr, n, str){
  return Array(n-String(nr).length+1).join(str||'0')+nr;
}

function queryInicioSI(connection,agent)  {
    let numerocop = agent.parameters.numerocop.toString();
    let numerodni = agent.parameters.numerodni.toString();
    if (numerocop.length===5){
    } else {
      if (numerocop.length<5){
        numerocop=padLeft(agent.parameters.numerocop,5,'0')
      }
    }

    return new Promise( (resolve,reject)=>{
      setTimeout(async ()=> {       
        const resultado = await connection.executeQuery("select  * from dbo.Colegiado where REPLACE(LTRIM(REPLACE(LE,'0',' ')),' ','0') like @dni  and Id_codigo like @id",[
          {
            name: 'id',
            type: 'varchar',
            value: numerocop
          },
           {
            name: 'dni',
            type: 'varchar',
            value: numerodni
          }           
    
        ]);
        resolve(resultado.data);
        reject("error");
    
    },1000);                
      });
  }      

function EscribirEstadoConsulta(agent,estado)  {
  const numerocop = agent.parameters.numerocop;
  const numerodni = agent.parameters.numerodni;
  const descrip=agent.query;

  const fecha = new Date();
  let forfecha1=forfecha(fecha);

  
  const logger = fs.createWriteStream('consultas.csv', {
      flags: 'a' 
    })
    
    logger.write("\n"+numerocop+";"+numerodni+";"+descrip+";"+forfecha1+";"+estado+";"+agent.intent) 
    logger.end() 

}      

function forfecha(fecha) {
  let day = fecha.getDate()
  let month = fecha.getMonth() + 1
  let year = fecha.getFullYear()
  let forfecha="";
  if(month < 10){
    forfecha=`${day}/0${month}/${year}`;
  }else{
    forfecha=`${day}/${month}/${year}`;
  }
  return forfecha;  
}


function querySolicitudEstadoBeneficio(connection,agent,tipobeneficio)  {
  let numerocop = agent.parameters.numerocop.toString();
  if (numerocop.length===5){
  } else {
    if (numerocop.length<5){
      numerocop=padLeft(agent.parameters.numerocop,5,'0')
    }
  }  
  return new Promise( (resolve,reject)=>{
    setTimeout(async ()=> {       

      const resultado = await connection.executeQuery("SELECT TOP 1 CONVERT(VARCHAR(10), Fe_Solicitud, 111) as Fe_Solicitud1,CONVERT(VARCHAR(10), Fe_Pago, 111) as Fe_Pago1,CONVERT(VARCHAR(10), Fe_Aprobacion, 111) as Fe_Aprobacion1,* FROM SolicitudBeneficio INNER JOIN Beneficio ON SolicitudBeneficio.Id_Exped = Beneficio.Id_Exped and SolicitudBeneficio.Nu_Corre = Beneficio.nu_corre  where Id_Beneficio like '%"+tipobeneficio+"%' and Id_codigo=@id order by SolicitudBeneficio.Nu_Corre DESC ",[{
          name: 'id',
          type: 'varchar',
          value: numerocop
      }
  
      ]);
      resolve(resultado.data);
      reject("error");
  
  },1000);                
    });
}      


function querycuenta(connection,agent)  {
  let numerocop = agent.parameters.numerocop.toString();
  if (numerocop.length===5){
  } else {
    if (numerocop.length<5){
      numerocop=padLeft(agent.parameters.numerocop,5,'0')
    }
  }  
  return new Promise( (resolve,reject)=>{
    setTimeout(async ()=> {       

      const resultado = await connection.executeQuery("select TOP 1 SUM(dbo.abono_colegio.Importe) OVER(ORDER BY dbo.abono_colegio.Importe ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS Deuda,* from dbo.abono_colegio INNER JOIN dbo.Colegio ON  dbo.abono_colegio.Id_Colegio=dbo.Colegio.Id_Colegio where dbo.abono_colegio.Id_Codigo=@id and Id_Abono>= '200001' AND Id_Abono<= '202112' and In_pago='N'  order by Deuda DESC",[{
          name: 'id',
          type: 'varchar',
          value: numerocop
      }
  
      ]);
      resolve(resultado.data);
      reject("error");
  
  },1000);                
    });
}      
module.exports.conexionsqlserver = conexionsqlserver;
module.exports.querySolicitudEstadoBeneficio = querySolicitudEstadoBeneficio;
module.exports.querycuenta = querycuenta;
module.exports.queryInicioSI = queryInicioSI;
module.exports.EscribirEstadoConsulta = EscribirEstadoConsulta;
module.exports.forfecha = forfecha;


