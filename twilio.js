require('dotenv').config();
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

function sendTextMessage(sender,message) {
    return new Promise( (resolve,reject)=>{
        client.messages
        .create({
           from: 'whatsapp:+14155238886',
           body: message,
           to: "whatsapp:+"+sender
         })
        .then((message) => resolve())
        .catch((err)=>reject(err));
        ;        

    });
}

//sendTextMessage("51929663244","mensaje de prueba");
module.exports={
    sendTextMessage,
};