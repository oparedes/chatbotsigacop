
const {Card,Suggestion,Payload} = require('dialogflow-fulfillment');

const axios=require("axios");
const fs=require('fs');
const https=require('https');
require('dotenv').config();

const twilio = require('./twilio');
const dialogflow=require("./dialogflow");
const express = require('express');

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended:true}));
const {WebhookClient} = require('dialogflow-fulfillment');

let consultasql = require('./consultasql');

const httpsServerOptions = {
    key: fs.readFileSync(process.env.KEY_PATH),
    cert: fs.readFileSync(process.env.CERT_PATH),
};


const serverHttps=https.createServer(httpsServerOptions,app);
serverHttps.listen(process.env.HTTPS_PORT,process.env.IP);


 
app.get('/', function (req, res) {
  res.send('Servidor Iniciado en node JS')
})

app.post('/webhook', express.json(),function (req, res) {
    const agent = new WebhookClient({ request:req, response:res });
    //console.log('Dialogflow Request headers: ' + JSON.stringify(req.headers));
    //console.log('Dialogflow Request body: ' + JSON.stringify(req.body));
   
    function welcome(agent) {
      agent.add(`Welcome to my agent!`);
    }

    function fallback(agent) {
      agent.add(`Pruebas de maternidad1`);

    }    
   
      
      function Modulowebhookfallback(agent) {
        consultasql.EscribirEstadoConsulta(agent,"NO");
        agent.add("No logro comprender la pregunta, estoy en proceso de aprendizaje,  ¿Podría indicar nuevamente cuál es su consulta.?");
      }

      function Moduloswebhooksi(agent) {
        console.log(agent.parameters.numerocop);
        agent.add("Indique el área con la que desea comunicarse: ");    
        if ((agent.requestSource)==="TELEGRAM") {
          AgregaModulosTelegram();
        } else if ((agent.requestSource)==="FACEBOOK") {
          AgregaModulosFacebbok();
                   
        } else {
          agent.add("Indique el área con la que desea comunicarse: Decanato, Mesa de partes, Administración,Economía,Logística, Planificación,FPS,Sistemas,Asesoría Legal o Colegiatura.");              
        }

        let context2 = new Object();
        context2 = {'name': 'iniciosi-followup', 'lifespan': 40, 'parameters': 
        {
          "numerocop": agent.parameters.numerocop,
          "numerodni": agent.parameters.numerodni
        }}; 

        context2.parameters.new = false;
        context2.parameters.refresh = true;
        agent.context.set(context2);                    

  
      }    

      function NumeracionEncuesta(pregunta) {
        let retorno=1;
        if (pregunta.toUpperCase()==="SIEMPRE") {
            retorno=1;
        } else if (pregunta.toUpperCase()==="CASI SIEMPRE") {
          retorno=2;
        } else if(pregunta.toUpperCase()==="A VECES") {
          retorno=3;
        } else if(pregunta.toUpperCase()==="NUNCA") {
          retorno=5;
        } else {
          retorno=4;
        }
        return retorno;
      }

      function Nivelsatisfaccion(agent) {
        let pregunta1 = NumeracionEncuesta(agent.parameters.pregunta1);
        let pregunta2 = NumeracionEncuesta(agent.parameters.pregunta2);
        let pregunta3 = NumeracionEncuesta(agent.parameters.pregunta3);
        let pregunta4 = NumeracionEncuesta(agent.parameters.pregunta4);
        let pregunta5 = NumeracionEncuesta(agent.parameters.pregunta5);
        let pregunta6 = NumeracionEncuesta(agent.parameters.pregunta6);
        let pregunta7 = NumeracionEncuesta(agent.parameters.pregunta7);
        let pregunta8 = NumeracionEncuesta(agent.parameters.pregunta8);
        let pregunta9 = NumeracionEncuesta(agent.parameters.pregunta9);
        let pregunta10 = NumeracionEncuesta(agent.parameters.pregunta10);
        let pregunta11 = NumeracionEncuesta(agent.parameters.pregunta11);
        let pregunta12 = NumeracionEncuesta(agent.parameters.pregunta12);

        const identificador = Date.now();
        const fecha1 = new Date();
        const fecha = consultasql.forfecha(fecha1);
        axios.post("https://sheet.best/api/sheets/ac0c5951-befa-477d-83e9-7c5aee63a6d5",{identificador,pregunta1,pregunta2,pregunta3,pregunta4,pregunta5,pregunta6,pregunta7,pregunta8,pregunta9,pregunta10,pregunta11,pregunta12,fecha});
        agent.add(`Gracias por utilizar nuestro servicio, fue un placer atenderlo`);
      }    
      
      function InicioSI(agent) {
        const numerocop = agent.parameters.numerocop;
        const numerodni = agent.parameters.numerodni;        
        return consultasql.queryInicioSI(consultasql.conexionsqlserver(),agent)
        .then(result=>{
          if ( Object.keys(result[0]).length === 0 ) {
            agent.add("Lo siento, el numero de colegiatura digitado no se encuentra en el sistema, corregir");
            agent.setFollowupEvent('errorcopdni');            
            let context2 = new Object();
            context2 = {'name': 'Inicio-followup', 'lifespan': 40, 
            'parameters': {}}; 
            console.log("ctx: = " + JSON.stringify(context2));
            context2.parameters.new = false;
            context2.parameters.refresh = true;
            agent.context.set(context2);            

          } else {
            
            if ((agent.requestSource)==="TELEGRAM") {
              agent.add("Muchas gracias "+result[0][0].nom_pat+" "+result[0][0].nom_mat+" "+result[0][0].Nombres+"  por la información brindada.  Indique el área con la que desea comunicarse:");
              AgregaModulosTelegram();

            } else if ((agent.requestSource)==="FACEBOOK") {
              agent.add("Muchas gracias "+result[0][0].nom_pat+" "+result[0][0].nom_mat+" "+result[0][0].Nombres+"  por la información brindada.  Indique el área con la que desea comunicarse:");
              AgregaModulosFacebbok();
                       
            } else {

              agent.add("Muchas gracias "+result[0][0].nom_pat+" "+result[0][0].nom_mat+" "+result[0][0].Nombres+"  por la información brindada.  Indique el área con la que desea comunicarse: Decanato, Mesa de partes, Administración,Economía,Logística, Planificación,FPS,Sistemas,Asesoría Legal o Colegiatura.");              
            }
          }
        });        
      }            


function AgregaModulosTelegram() {
  agent.add(new Suggestion("Decanato"));
  agent.add(new Suggestion("Mesa de partes"));
  agent.add(new Suggestion("Administración"));
  agent.add(new Suggestion("Economía"));
  agent.add(new Suggestion("Logística"));
  agent.add(new Suggestion("Planificación"));
  agent.add(new Suggestion("FPS"));
  agent.add(new Suggestion("Sistemas"));
  agent.add(new Suggestion("Asesoría Legal"));
  agent.add(new Suggestion("Colegiatura"));                
}
function AgregaSiTelegram() {
  agent.add(new Suggestion("SI"));
  agent.add(new Suggestion("NO"));
}
function AgregaSiFacebook(){
  agent.add(
    new Payload(agent.FACEBOOK,
    {
      "text": "Escoja una opción:",
      "quick_replies":[
        {
          "content_type":"text",
          "title":"SI",
          "payload":"SI",
          "image_url":""
        },

        {
          "content_type":"text",
          "title":"NO",
          "payload":"NO",
          "image_url":""
        }                                                                                                                                                                                                                                                                                            
      ]
    },
    {
      sendAsMessage: true,
      rawPayload: false,
    })
  );                          
}

function AgregaModulosFacebbok() {
  agent.add(
    new Payload(agent.FACEBOOK,
    {
      "text": "Escoja una opción:",
      "quick_replies":[
        {
          "content_type":"text",
          "title":"Decanato",
          "payload":"Decanato",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"Mesa de partes",
          "payload":"Mesa de partes",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"Administración",
          "payload":"Administración",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"Economía",
          "payload":"Economía",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"Logística",
          "payload":"Logística",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"Planificación",
          "payload":"Planificación",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"FPS",
          "payload":"FPS",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"Sistemas",
          "payload":"Sistemas",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"Asesoría Legal",
          "payload":"Asesoría Legal",
          "image_url":""
        },
        {
          "content_type":"text",
          "title":"Colegiatura",
          "payload":"Colegiatura",
          "image_url":""
        }                                                                                                                                                                                                                                                                                            
      ]
    },
    {
      sendAsMessage: true,
      rawPayload: false,
    })
  );   
}

//
function Estadodecuenta(agent) {
  return querySQLcuenta(agent);        
}      

function EstadoSolicitudBeneficioBeter(agent) {
  const tipobeneficio="201";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioAyuda(agent) {
  const tipobeneficio="202";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioInvalidez(agent) {
  const tipobeneficio="102";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioBefac(agent) {
  const tipobeneficio="200";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioFer(agent) {
  const tipobeneficio="108";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioFallecimiento(agent) {
  const tipobeneficio="103";
  return querySQL(agent,tipobeneficio);        
}      

function EstadoSolicitudBeneficioEnfermedad(agent) {
  const tipobeneficio="101";
  return querySQL(agent,tipobeneficio);        
}      
function EstadoSolicitudBeneficioMaternidad(agent) {
  const tipobeneficio="107";
  return querySQL(agent,tipobeneficio);        
}
////////////////////////////
function querySQLcuenta(agent) {
  return  consultasql.querycuenta(consultasql.conexionsqlserver(),agent)
    .then(result=>{
      if ( Object.keys(result[0]).length === 0 ) {
          agent.add("Usted no Tiene deudas pendientes. Se encuentra HABILITADO");
          agent.add("Desea volver al inicio para continuar con otra consulta");  
          if ((agent.requestSource)==="TELEGRAM") {
            AgregaSiTelegram();
          } else if ((agent.requestSource)==="FACEBOOK") {
            AgregaSiFacebook();
          } else {
          }        
  
      } else {
        //if (result[0][0].estado==='P') {
          agent.add("Usted tiene una deuda de  : "+result[0][0].Deuda+".00  .Se encuentra INHABILITADO");
          agent.add(" Por favor cancelar en su región "+result[0][0].Tx_Colegio);
        //}
        agent.add("Desea volver al inicio para continuar con otra consulta");  
        if ((agent.requestSource)==="TELEGRAM") {
          AgregaSiTelegram();
        } else if ((agent.requestSource)==="FACEBOOK") {
          AgregaSiFacebook();
        } else {
        }
      }
    });  
  }
///////////////////////      

function querySQL(agent,tipobeneficio) {
return  consultasql.querySolicitudEstadoBeneficio(consultasql.conexionsqlserver(),agent,tipobeneficio)
  .then(result=>{

    if ( Object.keys(result[0]).length === 0 ) {
        agent.add("No se ha encontrado información en el tipo de beneficio solicitado");
        consultasql.EscribirEstadoConsulta(agent,"NO");
        agent.add("Desea volver al inicio para continuar con otra consulta");  
        if ((agent.requestSource)==="TELEGRAM") {
          AgregaSiTelegram();
        } else if ((agent.requestSource)==="FACEBOOK") {
          AgregaSiFacebook();
        } else {
        }        

    } else {
      consultasql.EscribirEstadoConsulta(agent,"SI");
      if (result[0][0].estado==='P') {
        agent.add("El estado de su solicitud está PENDIENTE con fecha de solicitud : "+result[0][0].Fe_Solicitud1+"  "+result[0][0].Tx_Descrip);
      }
      if (result[0][0].estado==='X') {
        agent.add("El estado de su solicitud está PENDIENTE con fecha de solicitud : "+result[0][0].Fe_Solicitud1+"  "+result[0][0].Tx_Descrip+"  Desea volver al inicio para realizar otra consulta?");
      }      
      if (result[0][0].estado==='V') {
        agent.add("El estado de su solicitud está en estado DEVUELTO con fecha de solicitud : "+result[0][0].Fe_Solicitud1+"  "+result[0][0].Tx_Descrip);
      }            
      if (result[0][0].estado==='A') {
        agent.add("El estado de su solicitud está APROBADO con fecha de solicitud  : "+result[0][0].Fe_Solicitud1+" Fecha de pago: "+result[0][0].Fe_Pago1+" "+result[0][0].Tx_Descrip+" Importe: "+result[0][0].Importe);
      }      
      if (result[0][0].estado==='D') {
        agent.add("El estado de su solicitud está DENEGADO con fecha de solicitud : "+result[0][0].Fe_Solicitud1+" "+result[0][0].Tx_Descrip);
      }          
      agent.add("Desea volver al inicio para continuar con otra consulta");  
      if ((agent.requestSource)==="TELEGRAM") {
        AgregaSiTelegram();
      } else if ((agent.requestSource)==="FACEBOOK") {
        AgregaSiFacebook();
      } else {
      }


/**/


    }
  });  
}



    let intentMap = new Map();
    intentMap.set('Default Welcome Intent', welcome);
    intentMap.set('Default Fallback Intent', fallback);

    intentMap.set('BienvenidaSI',InicioSI);
    intentMap.set('Pregunta12',Nivelsatisfaccion)

    intentMap.set('decanatosi',Moduloswebhooksi)
    intentMap.set('Administracionsi',Moduloswebhooksi) 
    intentMap.set('Asesorialegalsi',Moduloswebhooksi) 
    intentMap.set('Colegiaturasi',Moduloswebhooksi) 
    intentMap.set('Economiasi',Moduloswebhooksi)
    intentMap.set('Economiawebhook',Estadodecuenta)
    intentMap.set('Economiawebhooksi',Moduloswebhooksi)
    intentMap.set('Logisticasi',Moduloswebhooksi)
    intentMap.set('Mesadepartessi',Moduloswebhooksi)
    intentMap.set('Planificacionsi',Moduloswebhooksi)
    intentMap.set('Sistemassi',Moduloswebhooksi)

    intentMap.set('Maternidadwebhook',EstadoSolicitudBeneficioMaternidad);//
    intentMap.set('Maternidadwebhooksi',Moduloswebhooksi)
    intentMap.set('Maternidadwebhookfallback',Modulowebhookfallback)

    
    intentMap.set('Enfermedadwebhook',EstadoSolicitudBeneficioEnfermedad);//
    intentMap.set('Enfermedadwebhooksi',Moduloswebhooksi)
    intentMap.set('Enfermedadwebhookfallback',Modulowebhookfallback)

    intentMap.set('Fallecimientowebhook',EstadoSolicitudBeneficioFallecimiento);//
    intentMap.set('Fallecimientowebhooksi',Moduloswebhooksi)
    intentMap.set('Fallecimientowebhookfallback',Modulowebhookfallback)


    intentMap.set('Ferwebhook',EstadoSolicitudBeneficioFer);//
    intentMap.set('Ferwebhooksi',Moduloswebhooksi)
    intentMap.set('Ferwebhookfallback',Modulowebhookfallback)    

    intentMap.set('Befacwebhook',EstadoSolicitudBeneficioBefac);//
    intentMap.set('Befacwebhooksi',Moduloswebhooksi)
    intentMap.set('Befacwebhookfallback',Modulowebhookfallback)    
    
    
    intentMap.set('Invalidezwebhook',EstadoSolicitudBeneficioInvalidez);//
    intentMap.set('Invalidezwebhooksi',Moduloswebhooksi)
    intentMap.set('Invalidezwebhookfallback',Modulowebhookfallback)        


    intentMap.set('Beterwebhook',EstadoSolicitudBeneficioBeter);//    
    intentMap.set('Beterwebhooksi',Moduloswebhooksi)
    intentMap.set('Beterwebhookfallback',Modulowebhookfallback)            

    intentMap.set('Ayudasolidariawebhook',EstadoSolicitudBeneficioAyuda);//    
    intentMap.set('Ayudasolidariawebhooksi',Moduloswebhooksi)
    intentMap.set('Ayudasolidariawebhookfallback',Modulowebhookfallback)            

    agent.handleRequest(intentMap);            

  })


//Whatsapp
app.post('/webhookwhatsapp',async function (req,res) {
    console.log(JSON.stringify(req.body,null," "));

    let phone=req.body.WaId;
    let receivedMessage=req.body.Body;

    let payload = await dialogflow.sendToDialogFlow(receivedMessage,"AAA");
    let responses = payload.fulfillmentMessages;
    //console.log(JSON.stringify(responses,null," "));
    for (const response of responses) {
      if (response.platform==="PLATFORM_UNSPECIFIED") {
      await twilio.sendTextMessage(phone,response.text.text[0]);
      }
      //await twilio.sendTextMessage(phone,response.quickReplies.title);
    }
    //twilio.sendTextMessage(req.body.WaId,req.body.Body);
    //res.status(200).json({ok:true,message:"Enviado correctamente"});
});
//Fin whatsapp  
 
//app.listen(3000,()=>{console.log("Servidor Iniciado puerto 3000");})//Local con ngrok
app.listen(80,()=>{console.log("Servidor Iniciado puerto 80");})//Amazon Server  
